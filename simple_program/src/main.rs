fn is_triangular(start_row: i64, test_value: i64) -> bool {
    if test_value < 0 {
        return false;
    }

    let mut sum = start_row;
    let mut i = 1;
    while sum < test_value {
        sum += i;
        i += 1;
    }

    sum == test_value
}

fn main() {
    let mut count = 0;
    for i in 100000000..110000000 {
        if is_triangular(5, i) {
            count += 1;
        }
    }
    println!("{}", count);
}
