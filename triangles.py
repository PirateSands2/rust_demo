import math


def is_triangular(start_row, val):
    if val < 0:
        return false

    sum_ = start_row
    i = 0
    while sum_ < val:
        sum_ += i
        i += 1

    return sum == val


def main():
    count = 0
    for i in range(100000000, 110000000):
        if is_triangular(5, i):
            count += 1

    print(count)


if __name__ == '__main__':
    main()
