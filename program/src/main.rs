use std::sync::{mpsc, Arc, Mutex};
use std::thread;

#[derive(Debug)]
struct Cache {
    trapezoidal_numbers: Vec<i64>,
}

impl Cache {
    pub fn new() -> Self {
        Self {
            trapezoidal_numbers: vec![],
        }
    }

    pub fn insert(&mut self, value: i64) {
        self.trapezoidal_numbers.push(value);
    }

    pub fn count(&self) -> usize {
        self.trapezoidal_numbers.len()
    }
}

fn is_triangular(start_value: i64, test_value: i64) -> bool {
    if test_value < 0 {
        return false;
    }

    let mut sum = start_value;
    let mut i = 1;
    while sum < test_value {
        sum += i;
        i += 1;
    }

    sum == test_value
}

fn main() {
    let cache = Arc::new(Mutex::new(Cache::new()));

    let num_threads = 12;
    let mut handles = vec![];

    for _ in 0..num_threads {
        let (tx1, rx1) = mpsc::channel();
        let (tx2, rx2) = mpsc::channel();

        let cloned_cache = cache.clone();

        let handle = thread::spawn(move || loop {
            let (start_value, test_value) = match rx1.recv() {
                Ok(val) => val,
                Err(e) => panic!("Receive failed: {}", e),
            };

            let res = is_triangular(start_value, test_value);

            if res {
                cloned_cache.lock().unwrap().insert(test_value);
            }

            tx2.send(res).expect("TX failed");
        });

        handles.push((handle, tx1, rx2));
    }

    let mut i = 100000000;
    let end_val = 110000000;
    let row_start = 5;

    for (_, tx, _) in handles.iter() {
        tx.send((row_start, i)).expect("TX Failed");
        i += 1;
    }

    loop {
        for (_, tx, rx) in handles.iter() {
            match rx.recv_timeout(std::time::Duration::from_micros(10)) {
                Ok(_) => {
                    tx.send((row_start, i)).expect("TX failed");
                    i += 1;
                }
                Err(_) => {}
            }
        }
        if i >= end_val {
            println!("{:?}", cache.lock().expect("Mutex poisoned").count());
            std::process::exit(0);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_is_trapezoidal() {
        let test_values = vec![
            (5, 5, true),
            (5, 11, true),
            (3, 3, true),
            (3, 7, true),
            (3, 0, false),
            (0, -1, false),
            (1, -1, false),
        ];

        for (start_row, val, expected_res) in test_values {
            println!("{}, {}, {}", start_row, val, expected_res);
            assert_eq!(is_triangular(start_row, val), expected_res);
        }
    }
}
